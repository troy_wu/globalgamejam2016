﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleScene : MonoBehaviour {

	public GameObject m_title;

	void Start()
	{	PlayerPrefs.SetInt("showInstructions", 1);
	}

	// Update is called once per frame
	void Update () {
		if(!LeanTween.isTweening (m_title) && Input.GetMouseButtonDown(0))
		{	if(m_title.activeSelf)
			{	LeanTween.moveY(m_title, m_title.transform.position.y+5, 0.5f).setOnComplete(LoadNextScene);
			}
		}
	}

	void LoadNextScene()
	{	m_title.SetActive(false);
		SceneManager.LoadScene ("DogdaysNew");
	}
}
