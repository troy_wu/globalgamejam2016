﻿using UnityEngine;
using System.Collections;

public class GameScene : MonoBehaviour {
	public GameObject mCharacter;
	public GameObject mDarkPanel;

	public GameObject mHouse;
	private bool isPlayFireWork = false;
	float mTimer = 0;

	[HideInInspector] public AudioClip m_themeMusic01;
	[HideInInspector] public AudioClip m_themeMusic02;
	[HideInInspector] public AudioClip m_themeMusic03;
	[HideInInspector] public AudioClip m_themeMusic04;
	[HideInInspector] public AudioClip m_themeMusic05;

	[HideInInspector] public AudioClip m_firework;

	void Start () {
		mTimer = Time.time;
		mDarkPanel.SetActive (true);
		LeanTween.alpha (mDarkPanel, 0, 1);
	}
	void Awake () {
		m_themeMusic01 = OGResourceMgr.Instance.LoadAudioClip("Audio/ThemeMusic/thememusic001");
		m_themeMusic02 = OGResourceMgr.Instance.LoadAudioClip("Audio/ThemeMusic/thememusic002");
		m_themeMusic03 = OGResourceMgr.Instance.LoadAudioClip("Audio/ThemeMusic/thememusic003");
		m_themeMusic04 = OGResourceMgr.Instance.LoadAudioClip("Audio/ThemeMusic/thememusic004");
		m_themeMusic05 = OGResourceMgr.Instance.LoadAudioClip("Audio/ThemeMusic/thememusic005");
		m_firework = OGResourceMgr.Instance.LoadAudioClip("Audio/fireworks");

		int random = Random.Range(0,4);

		switch (random) {
		case 0:
			OGAudioMgr.Instance.PlayBgm(m_themeMusic01);
			break;
		case 1:
			OGAudioMgr.Instance.PlayBgm(m_themeMusic02);
			break;
		case 2:
			OGAudioMgr.Instance.PlayBgm(m_themeMusic03);
			break;
		case 3:
			OGAudioMgr.Instance.PlayBgm(m_themeMusic04);
			break;
		case 4:
			OGAudioMgr.Instance.PlayBgm(m_themeMusic05);
			break;
		default:
			OGAudioMgr.Instance.PlayBgm(m_themeMusic01);
			break;
		}

		if(PlayerPrefs.GetInt("showInstructions") == 0)
		{
			mDarkPanel.SetActive (true);
			LeanTween.alpha (mDarkPanel, 0, 1);
		}
	}
	

	void Update () {
		if (isPlayFireWork) {
			if (Time.time - mTimer > 1) {
				mTimer = Time.time + Random.Range (0, 0.8f);
				LoadFireWork ();
			}
		}
	}

	public void PlayFireWork(){
		OGAudioMgr.Instance.PlaySfx(m_firework);
		isPlayFireWork = true;
	}

	private void LoadFireWork(){
		GameObject firework = Instantiate (Resources.Load ("FireWork", typeof(GameObject))) as GameObject;
		firework.transform.position = new Vector3 (mHouse.transform.position.x + Random.Range(-3f,3f), mHouse.transform.position.y);
	}

	public void OnRightPanelPointDown(){
		mCharacter.GetComponent<dog>().StartWlak();
	}
	public void OnRightPanelPointUp(){
		mCharacter.GetComponent<dog>().StopWlak();
	}
	public void OnLeftPanelClick(){
		mCharacter.GetComponent<dog> ().Bark ();
	}
}
