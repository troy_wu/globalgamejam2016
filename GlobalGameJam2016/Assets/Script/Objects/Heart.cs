﻿using UnityEngine;
using System.Collections;

public class Heart : MonoBehaviour {

	public GameObject mCharacter;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void show () {
		gameObject.GetComponent<SpriteRenderer> ().enabled = true;
		mCharacter.GetComponent<dog>().setIsStucking (true);

		LeanTween.scale (gameObject, new Vector3 (9, 9, 9), 1.5f);
		LeanTween.alpha (gameObject, 0, 1).setDelay (1).setOnComplete(onAnimOver) ;
	}

	private void onAnimOver() {
		mCharacter.GetComponent<dog>().setIsStucking (false);
		gameObject.SetActive (false);
	}
}

