﻿using UnityEngine;
using System.Collections;

public class Tree : MonoBehaviour {

	public GameObject mLeaves;
	public GameObject mTrunk;
	public GameObject mCollars;

	public GameObject mBird;
	[HideInInspector] public AudioClip m_collerDropSFX;

	// Use this for initialization
	void Start () {
		m_collerDropSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/項圈掉下003");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D obj){
		if(obj.tag =="DeadTrigger"){
//			m_gameScene.GetComponent<PinballGameScene>().EndGame();
		}
	}

	public void OnDogBrak(){
		Debug.Log ("Tree OnDogBrak");
		if (!LeanTween.isTweening (mLeaves)) {
			LeanTween.moveX (mLeaves, mLeaves.transform.position.x + 0.5f, 0.05f).setRepeat (10).setLoopPingPong ();
		}
		if (!LeanTween.isTweening (mCollars) && mCollars.transform.position.y > mTrunk.transform.position.y) {
			LeanTween.moveY (mCollars, mCollars.transform.position.y - 5, 1).setOnComplete(()=>{
				OGAudioMgr.Instance.PlaySfx(m_collerDropSFX);
			});
		}

		if (!LeanTween.isTweening(mBird)) {
			LeanTween.moveX (mBird, mBird.transform.position.x + 3, 0.5f);
			LeanTween.moveY (mBird, mBird.transform.position.y + 4, 0.5f);

			// Vector3[] vectorArray = new Vector3[3];
			// vectorArray [0] = new Vector3 (mBird.transform.position.x + 1, mBird.transform.position.y, mBird.transform.position.z);
			// vectorArray [1] = new Vector3 (mBird.transform.position.x + 1, mBird.transform.position.y + 1, mBird.transform.position.z);
			// vectorArray [2] = new Vector3 (mBird.transform.position.x + 1, mBird.transform.position.y + 2, mBird.transform.position.z);
			// LeanTween.moveSpline (mBird, vectorArray, 1).setEase (LeanTweenType.easeOutQuart);

		}
	}



}
