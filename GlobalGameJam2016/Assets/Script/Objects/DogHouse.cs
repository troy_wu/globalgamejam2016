﻿using UnityEngine;
using System.Collections;

public class DogHouse : MonoBehaviour {
	private bool isLocked = true;
	public GameObject house;
	public GameObject houseLocked;
	// Use this for initialization
	void Start () {
		house.SetActive (false);
		houseLocked.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SetLocked(bool locked){
		isLocked = locked;
		house.SetActive (!locked);
		houseLocked.SetActive (locked);
	}

	public bool GetInHouse(){
		if (isLocked)
			return false;
		house.GetComponent<Animator> ().SetTrigger ("GoHouse");
		return true;
	}
}
