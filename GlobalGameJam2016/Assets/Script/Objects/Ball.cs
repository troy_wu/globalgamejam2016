﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public GameObject mTrashCan;

	public GameObject mTrashCanBody;
	public GameObject mTrashCamCover;

	public GameObject mGirlDog;
	public GameObject mHeart;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnDogKick(){
		if (!LeanTween.isTweening (gameObject) && mGirlDog.activeSelf) {
			LeanTween.moveLocalX (gameObject, mTrashCan.transform.position.x - gameObject.GetComponent<SpriteRenderer> ().bounds.size.x, 3);
			LeanTween.moveLocalY (gameObject, gameObject.transform.position.y + 10, 1.5f).setRepeat (2).setLoopPingPong ().setOnComplete (onAnimationFinish);

		}
	}


	private void onAnimationFinish() {
		mGirlDog.SetActive (false);
		mHeart.SetActive (false);

		mTrashCanBody.transform.rotation = Quaternion.Euler (0, 0, -90);
		float x = mTrashCanBody.transform.position.x;
		float y = mTrashCanBody.transform.position.y;
		float z = mTrashCanBody.transform.position.z;
		mTrashCanBody.transform.position = new Vector3 (x + 0.5f, y + 0.1f, z);

		mTrashCamCover.transform.rotation = Quaternion.Euler (0, 0, -170);
		x = mTrashCamCover.transform.position.x;
		y = mTrashCamCover.transform.position.y;
		z = mTrashCamCover.transform.position.z;
		mTrashCamCover.transform.position = new Vector3 (x + 3.3f, y - 1.9f, z);
	}
}
