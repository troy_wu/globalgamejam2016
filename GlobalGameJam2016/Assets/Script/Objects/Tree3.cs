﻿using UnityEngine;
using System.Collections;

public class Tree3 : MonoBehaviour {

	public GameObject mLeaves;
	public GameObject mTrunk;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D obj){
		if(obj.tag =="DeadTrigger"){
//			m_gameScene.GetComponent<PinballGameScene>().EndGame();
		}
	}

	public void OnDogBrak(){
		Debug.Log ("Tree OnDogBrak");
		if (!LeanTween.isTweening (mLeaves)) {
			LeanTween.moveX (mLeaves, mLeaves.transform.position.x + 0.3f, 0.03f).setRepeat (10).setLoopPingPong ();
		}
	}

}
