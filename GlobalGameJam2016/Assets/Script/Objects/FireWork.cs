﻿using UnityEngine;
using System.Collections;

public class FireWork : MonoBehaviour {
	public GameObject mObj;
	public GameObject mParticalSystem;
	// Use this for initialization
	void Start () {
		LeanTween.moveLocalY (gameObject, transform.position.y + Random.Range(4f, 6f), 2f).setOnComplete(Explosion);
	}

	void Explosion()
	{
		mParticalSystem.SetActive (true);
		mObj.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
