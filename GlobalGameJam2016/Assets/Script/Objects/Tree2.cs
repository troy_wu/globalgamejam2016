﻿using UnityEngine;
using System.Collections;

public class Tree2 : MonoBehaviour {

	public GameObject mLeaves;
	public GameObject mTrunk;
	public GameObject mKite;

	bool m_kiteFly = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D obj){
		if(obj.tag =="DeadTrigger"){
//			m_gameScene.GetComponent<PinballGameScene>().EndGame();
		}
	}

	public void OnDogBrak(){
		Debug.Log ("Tree OnDogBrak");
		if (!LeanTween.isTweening (mLeaves)) {
			LeanTween.moveX (mLeaves, mLeaves.transform.position.x + 0.5f, 0.05f).setRepeat (10).setLoopPingPong ();
		}
		if (!LeanTween.isTweening (mKite) && !m_kiteFly) {
			m_kiteFly = true;
			LeanTween.move (mKite, new Vector3(mKite.transform.position.x+5, mKite.transform.position.y+5, mKite.transform.position.z), 1);
		}
	}

}
