﻿using UnityEngine;
using System.Collections;

public class SmoothCamera2D : MonoBehaviour {
	public float dogOffset = 4;
	public float dampTime = 0.05f;
	private Vector3 velocity = Vector3.zero;
	public Transform target;
	public Transform box;
//	public GameObject m_side;

	CAMERASTATUS m_status = CAMERASTATUS.Start;
	float marginY = 0;

	void Start()
	{	transform.position = new Vector3(target.position.x + dogOffset ,target.position.y + 3f , transform.position.z);//2f
	}

	void Update () 
	{
		if (box.position.x < transform.position.x)
			return;
		if (target && m_status == CAMERASTATUS.Start)
		{
			Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
			Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
			destination = new Vector3(destination.x + dogOffset ,destination.y + 3f ,destination.z);//2f
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
		}else if (target && m_status == CAMERASTATUS.InGame)
		{
			Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
			Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
			destination = new Vector3(0 ,destination.y + 2.8f,destination.z);//2f
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
		}else if(target && m_status == CAMERASTATUS.Cridits){
			transform.position = new Vector3(0,-10,-10);
		}else if(target && m_status == CAMERASTATUS.Spin){
			transform.position = new Vector3(0,-25,-10);
		}

		if (transform.position.y * (-0.2f) + marginY < -11.36f) {
			marginY += 11.36f;
		}
//		m_side.transform.localPosition = new Vector3 (0, transform.position.y*(-0.2f) + marginY, 0);
//
//		if (m_side.transform.localPosition.y > 0) { //return to start status
//			m_side.transform.localPosition = Vector3.zero;
//			marginY = 0;
//		}

	}

	public void setStatus(CAMERASTATUS status){
		m_status = status;
	}
}

public enum CAMERASTATUS{
	Start,
	InGame,
	Cridits,
	Spin
}