// Contributors: Derrick, Michelle, Joanna

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

	public class OGAudioMgr : Singleton<OGAudioMgr> 
	{
		AudioSource[] m_audioSources;
		int m_maxAudioCount = 16;
		
		bool m_sfxEnabled = true;
		bool m_bgmEnabled = true;
		
		float m_sfxVolume = 1;
		float m_bgmVolume = 1;
		
		public void Awake(){
			Init();
		}
		
		public void Init()
		{	
			for (int i=0; i< m_maxAudioCount; i++) 
			{	
				this.gameObject.AddComponent<AudioSource>();
			}
			
			m_audioSources = (AudioSource[]) this.gameObject.GetComponents<AudioSource>();
		}
		
		AudioSource GetAvailableSource() 
		{	
			for (int i=1; i< m_audioSources.Length; i++) 
			{	
				AudioSource audioSource = (AudioSource) m_audioSources[i];
				
				if(!audioSource.isPlaying)
				{	
					return audioSource;
				}
			}
			
			return m_audioSources[1]; // worst case, if all 16 streams are in use, we stop earliest stream.
		}
		
		AudioSource GetAudioSourceForClip(AudioClip audio) 
		{	
			for (int i=0; i< m_audioSources.Length; i++) 
			{	
				AudioSource audioSource = (AudioSource) m_audioSources[i];
				
				if(audioSource.clip == audio)
				{	
					return audioSource;
				}
			}
			
			return null;
		}
		
		//NOTE: Joanna: PlaySFX(AudioClip audio, "pitch",2f, "volume", 0.5f, "loop", true, "pan", -1f ); 
		public void PlaySfx(AudioClip audio, params object[] options)
		{
			PlaySfx( audio, Hash(options));
		}
		
		//NOTE: Joanna: PlaySFX(AudioClip audio, Hash( "pitch",2f, "volume", 0.5f, "loop", true, "pan", -1f ) ); 
		void PlaySfx(AudioClip audio, Hashtable options)
		{
			AudioSource audioSource = GetAvailableSource(); 
			
			if (audioSource != null)
			{
				audioSource.clip = audio;
				
				if (options.Contains("pitch")) 
				{
					audioSource.pitch = (float) options["pitch"]; 
				}
				else 
				{
					audioSource.pitch = 1f; 
				}
				
				if (options.Contains("volume")) 
				{
					audioSource.volume = (float) options["volume"]; 
				}
				else 
				{
					audioSource.volume = m_sfxVolume; 
				}
				
				if (options.Contains("loop")) 
				{
					audioSource.loop = (bool) options["loop"]; 
				}
				else 
				{
					audioSource.loop = false; 
				}
				
				if (options.Contains("pan")) 
				{
					audioSource.panStereo = (float) options["pan"]; 
				}
				else 
				{
					audioSource.panStereo = 0f; 
				}
				
				if( m_sfxEnabled )
					audioSource.Play ();
			}
		}
		
		public void PlayBgm(AudioClip audio, bool isLooping=true)
		{	
			AudioSource audioSource = m_audioSources[0];
			
			if (audioSource != null) 
			{	
				audioSource.clip = audio;
				audioSource.loop = isLooping;
				audioSource.pitch = 1f;
				audioSource.volume = m_bgmVolume;
				
				if( m_bgmEnabled )
					audioSource.Play ();
			}
		}
		
		public void SetBgmPitch(float pitch)
		{
			AudioSource audioSource = m_audioSources[0];
			
			if (audioSource != null) 
			{	
				audioSource.pitch = pitch;
			}
		}
		
		public void PauseSfx(AudioClip audio)
		{	
			if( !m_sfxEnabled )
				return;
			
			AudioSource audioSource = GetAudioSourceForClip(audio);
			
			if (audioSource != null)
			{	
				audioSource.Pause ();
			}
		}
		
		public void PauseBgm()
		{	
			AudioSource audioSource = m_audioSources[0];
			
			if (audioSource != null) 
			{	
				audioSource.Pause();
			}
		}
		
		public void PauseAll()
		{	
			for (int i=0; i< m_audioSources.Length; i++) 
			{
				AudioSource audioSource = (AudioSource)m_audioSources [i];
				audioSource.Pause();
			}
		}
		
		public void ResumeSfx(AudioClip audio)
		{	
			AudioSource audioSource = GetAudioSourceForClip(audio);
			
			if (audioSource != null) 
			{	
				audioSource.Play ();
			}
		}
		
		public void ResumeBgm()
		{	
			AudioSource audioSource = m_audioSources[0];
			
			if (audioSource != null) 
			{	
				audioSource.Play ();
			}
		}
		
		public void ResumeAll(AudioClip audio)
		{	
			for (int i=0; i< m_audioSources.Length; i++) 
			{
				AudioSource audioSource = (AudioSource)m_audioSources [i];
				audioSource.Play();
			}
		}
		
		
		public void StopSFX(AudioClip audio)
		{	
			AudioSource audioSource = GetAudioSourceForClip(audio);
			
			if (audioSource != null) 
			{	
				audioSource.Stop ();
			}
		}
		
		public void StopBgm()
		{	AudioSource audioSource = m_audioSources[0];
			
			if (audioSource != null) 
			{	
				audioSource.Stop();
			}
		}
		
		public void StopAllSfx()
		{
			for (int i=1; i< m_audioSources.Length; i++) 
			{
				AudioSource audioSource = (AudioSource)m_audioSources [i];
				audioSource.Stop();
			}
		}
		
		public void StopAll()
		{	
			for (int i=0; i< m_audioSources.Length; i++) 
			{
				AudioSource audioSource = (AudioSource)m_audioSources [i];
				audioSource.Stop();
			}
		}
		
		public void SetBGMVolume(float volume)
		{	m_bgmVolume = volume;
			
			AudioSource audioSource = m_audioSources[0];
			
			if (audioSource != null) 
			{	audioSource.volume = m_bgmVolume;
			}
		}
		
		public void SetSFXVolume(float volume)
		{	m_sfxVolume = volume;
			
			for (int i=1; i< m_audioSources.Length; i++) 
			{	AudioSource audioSource = (AudioSource)m_audioSources [i];
				
				if(audioSource != null)
				{	audioSource.volume = m_sfxVolume;
				}	
			}
		}
		
		public void EnabledBgm(bool isEnabled)
		{	
		if( m_audioSources == null || m_audioSources.Length <=0 )
				return;

			if( isEnabled )
			{
				m_bgmEnabled = true;
				
				AudioSource audioSource = m_audioSources[0];
				audioSource.Play();
			}
			else
			{
				m_bgmEnabled = false;
				
				AudioSource audioSource = m_audioSources[0];
				audioSource.Stop();
			}
		}
		
		public void EnabledSfx(bool isEnabled)
		{	
			if( isEnabled )
			{
				m_sfxEnabled = true;
			}
			else
			{
				m_sfxEnabled = false;
				
				for (int i=1; i< m_audioSources.Length; i++) 
				{
					AudioSource audioSource = (AudioSource)m_audioSources [i];
					audioSource.Stop();
				}
			}
		}
		
		public void EnabledAll(bool isEnabled)
		{	
			EnabledSfx(isEnabled);
			EnabledBgm(isEnabled);
		}
		

		Hashtable Hash( params object[] args )
		{	if(args.Length%2==1){
				//			Debug.Log(OGLogMgr.instance.format("OGAudioMgr", "Hash", "Hash requires an even number of arguments."));
				return null;
			}
			Hashtable hash = new Hashtable();
			for(int i = 0; i < args.Length; i += 2){
				hash.Add(args[i] as string, args[i+1]);
			}
			
			return hash;
		}

		public bool isBgmEnabled(){
			return m_bgmEnabled;
		}

		public bool isSfxEnabled(){
			return m_sfxEnabled;
		}
	}


