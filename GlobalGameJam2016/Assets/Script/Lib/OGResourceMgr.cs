﻿// Contributors: Michelle
using UnityEngine;
using System.Collections;

public class OGResourceMgr : Singleton<OGResourceMgr>
{
	public AudioClip LoadAudioClip (string clipName)
	{
		return (AudioClip)Resources.Load (clipName) as AudioClip;
	}
		
	public GameObject LoadPrefab (string prefabName)
	{
		return (GameObject)Resources.Load (prefabName);
	}
		
	public GameObject LoadPrefabAsGameObject (string prefabName)
	{
		return Instantiate (Resources.Load (prefabName)) as GameObject;
	}
		
	public Sprite LoadSprite (string spriteName)
	{
		return (Sprite)Resources.Load<Sprite> (spriteName);
	}
		
	public GameObject LoadSpriteObj (string spriteName)
	{	
		GameObject obj = new GameObject ();
		obj.name = spriteName;
		obj.AddComponent<SpriteRenderer> ();
		obj.GetComponent<SpriteRenderer> ().sprite = (Sprite)Resources.Load<Sprite> (spriteName);
		return obj;
	}
		
	// NOTE: Michelle: for Multiple Spritesheets:
	private Hashtable m_spritesCache;
		
	// GAME_TEAM: Michelle:spritsheet needs to be initialized first and foremost, initSpritesheet only accepts Multiple sprites
	public void InitSpritesheet (string spritesheetName)
	{		
		if (m_spritesCache == null) {
			m_spritesCache = new Hashtable ();		
		}
			
		Sprite[] spritesheet = Resources.LoadAll<Sprite> (spritesheetName);
			
		foreach (Sprite sprite in spritesheet) {	
			// sprites with same names are ignored
			if (!m_spritesCache.Contains (sprite.name)) {
				m_spritesCache.Add (sprite.name, sprite);
			}
		}
	}
		
	// GAME_TEAM: Michelle: always clear spritesheet cache when changing Scenes
	public void ClearSpritesheetCache ()
	{
		m_spritesCache.Clear ();
	}
		
	public Sprite LoadSpriteFromSpritesheet (string name)
	{
		if (m_spritesCache.Contains (name)) {
			return (Sprite)m_spritesCache [name];		
		}
			
		return null;
	}
		
	public GameObject LoadSpriteObjFromSpritesheet (string spriteName)
	{	
		GameObject obj = new GameObject ();
		obj.name = spriteName;
		obj.AddComponent<SpriteRenderer> ();
		obj.GetComponent<SpriteRenderer> ().sprite = LoadSpriteFromSpritesheet (spriteName);
		return obj;
	}
}

