﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class dog : MonoBehaviour {
	public GameObject mGameScene;

	public GameObject m_instructionWalk;
	public Image m_instructionWalkPanel;
	public GameObject m_instructionAction;
	public Image m_instructionActionPanel;
	bool m_willShowInstruction = false;

	public GameObject mTree;
	public GameObject mTree2;
	public GameObject mTree3;
	public GameObject mBall;
	public GameObject mTrashcan;
	public GameObject mPlank;
	public GameObject mDarkPanel;
	public GameObject mCollerOnDog;
	public GameObject mPuddleWaterLeft;
	public GameObject mPuddleWaterRight;
	public GameObject mSkateboard;
	public GameObject mSkateboardSkating;
	public GameObject mBox;
	public GameObject mBone;
	public GameObject mGirlDog;

	public GameObject mHeart;
	public GameObject mDouHouse;
	
	private float MoveSpeed = 0.1f;
	private bool isWalking = false;
	private bool mIsStucking = false;
	
	private bool isTriggerTree = false;
	private bool isTriggerTree2 = false;
	private bool isTriggerTree3 = false;
	private bool isTriggerBall = false;
	private bool isTriggerTrashcan = false;
	private bool isTriggerPlank = false;
	private bool isTriggerBone = false;
	private bool isTriggerPole = false;
	private bool isTriggerHouse = false;
	private bool isTriggerSkateboard = false;

	private bool isKickPlank = false;
	private bool isKickBall = false;
	private bool isSkating = false;

	[HideInInspector] public AudioClip m_boneSFX;
	[HideInInspector] public AudioClip m_skatingSFX;
	[HideInInspector] public AudioClip m_ballSFX;
	[HideInInspector] public AudioClip m_barkSFX;
	[HideInInspector] public AudioClip m_splashSFX;
	[HideInInspector] public AudioClip m_plankSFX;
	[HideInInspector] public AudioClip m_openBoxSFX;
	[HideInInspector] public AudioClip m_goodEndSFX;
	[HideInInspector] public AudioClip m_ihaveToPeeSFX;
	[HideInInspector] public AudioClip m_puddleSteoSFX;
	[HideInInspector] public AudioClip m_oohlalaSFX;
	[HideInInspector] public AudioClip m_needtogoSFX;
	[HideInInspector] public AudioClip m_weeSFX;


	// Use this for initialization
	void Start () {

		m_boneSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/bone002");
		m_skatingSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/滑板車");
		m_ballSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/ball002");
		m_barkSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/barking001");
		m_splashSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/splash001");
		m_plankSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/WoodDown002");
		m_openBoxSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/boxsadbox001");
		m_goodEndSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/homesweethome001");
		m_ihaveToPeeSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/ihavetopee001");
		m_puddleSteoSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/玩水大聲");
		m_oohlalaSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/oohlala002");
		m_needtogoSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/needtogo001");
		m_weeSFX = OGResourceMgr.Instance.LoadAudioClip("Audio/wee002");

		m_instructionWalk.SetActive(false);
		m_instructionAction.SetActive(false);

		if(PlayerPrefs.GetInt("showInstructions") == 1)
		{	m_willShowInstruction = true;

			PlayerPrefs.SetInt("showInstructions", 0);
			showWalkInstruction();
		}
	}
	
	void showWalkInstruction()
	{	m_instructionWalk.SetActive(true);
		m_instructionWalkPanel.color = new Color(1, 1, 1, 0.5f);
	}

	// Update is called once per frame
	void Update () {
		if (isWalking && !mIsStucking) {
			if(m_instructionWalk.activeSelf)
			{	m_instructionWalk.SetActive(false);
				m_instructionWalkPanel.color = new Color(1, 1, 1, 0);
			}

			gameObject.transform.position = new Vector3(gameObject.transform.position.x + MoveSpeed,gameObject.transform.position.y);	
		}
	}
	
	public void Walk(){
		
	}
	
	public void StartWlak(){
		isWalking = true;
		GetComponent<Animator> ().SetBool ("isWalking",true);
	}

	
	public void StopWlak(){
		isWalking = false;
		GetComponent<Animator> ().SetBool ("isWalking",false);
	}
	
	public void Bark(){
		OGAudioMgr.Instance.PlaySfx(m_barkSFX);

		if (isTriggerTree) {
			Invoke("BarkTree", 1);
			GetComponent<Animator>().SetTrigger("Bark");
		} else if (isTriggerBall && mGirlDog.activeSelf) {

			Invoke("KickBall", 1);
			isKickBall = true;
			GetComponent<Animator>().SetTrigger("Nudge");
		} else if (isTriggerPlank) {
			OGAudioMgr.Instance.PlaySfx(m_plankSFX);
			Invoke("KickPlank", 1);
			GetComponent<Animator>().SetTrigger("Nudge");
			isKickPlank = true;
			if (isKickBall && mCollerOnDog.activeSelf) {
				mDouHouse.GetComponent<DogHouse> ().SetLocked (false);
			}
		} else if (isTriggerTrashcan) {
		} else if (isTriggerBone) {
			Invoke("EatBone", 1);
			GetComponent<Animator>().SetTrigger("Eat");

			m_instructionAction.SetActive(false);
			m_instructionActionPanel.color = new Color(1, 1, 1, 0);
		} else if (isTriggerPole) {
			
			GetComponent<Animator>().SetTrigger("Pee");
		} else if (isTriggerTree2) {
			Invoke("BarkTree2", 1);
			OGAudioMgr.Instance.PlaySfx(m_weeSFX);
			GetComponent<Animator>().SetTrigger("Bark");
		}	else if (isTriggerTree3) {
			Invoke("BarkTree3", 1);
			GetComponent<Animator>().SetTrigger("Bark");
		} else if (isTriggerHouse) {
			OGAudioMgr.Instance.PlaySfx(m_goodEndSFX);
			if (mDouHouse.GetComponent<DogHouse> ().GetInHouse ()) {
				EndGame ();
			}
		} else if (isTriggerSkateboard) {
			SkateToTheEnd ();
		}
	}
	
	void KickBall()
	{	mBall.GetComponent<Ball> ().OnDogKick ();
	}
	
	void BarkTree()
	{	mTree.GetComponent<Tree> ().OnDogBrak ();
	}
	
	void BarkTree2()
	{	mTree2.GetComponent<Tree2> ().OnDogBrak ();
	}
	
	void BarkTree3()
	{	mTree3.GetComponent<Tree3> ().OnDogBrak ();
	}
	
	void KickPlank()
	{	mPlank.GetComponent<Plank>().OnDogKick();
	}
	
	void EatBone()
	{	mBone.SetActive(false);
		isTriggerBone = false;
	}
	
	public void OnTriggerEnter2D(Collider2D obj){
		if (obj.tag == "tree") {
			isTriggerTree = true;
		} else if (obj.tag == "ball") {
			OGAudioMgr.Instance.PlaySfx(m_ballSFX);
			isTriggerBall = true;
		} else if (obj.tag == "plank") {
			isTriggerPlank = true;
		} else if (obj.tag == "trashcan") {
			isTriggerTrashcan = true;
		} else if (obj.tag == "box") {
			OGAudioMgr.Instance.PlaySfx(m_openBoxSFX);
			OnTouchBox (obj.gameObject);
		} else if (obj.tag == "coller") {
			mCollerOnDog.SetActive (true);
			obj.gameObject.SetActive (false);
		} else if (obj.tag == "puddle") {
			if (!isKickPlank) {
				OGAudioMgr.Instance.PlaySfx(m_puddleSteoSFX);
				mPuddleWaterLeft.SetActive (true);
				mPuddleWaterRight.SetActive (true);
				GetComponent<SpriteRenderer> ().color = new Color (0.5f,0.5f,0.5f);
			}

		} else if (obj.tag == "Heart") {
			OGAudioMgr.Instance.PlaySfx(m_oohlalaSFX);
			if (!isSkating) {
				mHeart.GetComponent<Heart> ().show ();
			}
		} else if (obj.tag == "bone") {
			OGAudioMgr.Instance.PlaySfx(m_boneSFX);

			isTriggerBone = true;

			if (m_willShowInstruction) {
				m_instructionAction.SetActive (true);
				m_instructionActionPanel.color = new Color (1, 1, 1, 0.5f);
			}
		} else if (obj.tag == "pole") {
			int random = Random.Range(0,1);
			switch (random) {
			case 0: OGAudioMgr.Instance.PlaySfx(m_needtogoSFX); break;
			case 1: OGAudioMgr.Instance.PlaySfx(m_ihaveToPeeSFX); break;
			default: OGAudioMgr.Instance.PlaySfx(m_needtogoSFX); break;
			}
	
			isTriggerPole = true;
		} else if (obj.tag == "tree2") {
			isTriggerTree2 = true;
		} else if (obj.tag == "tree3") {
			isTriggerTree3 = true;
		} else if (obj.tag == "Heart") {
			
			mHeart.GetComponent<Heart> ().show ();
		} else if (obj.tag == "house") {
			isTriggerHouse = true;
		} else if (obj.tag == "Skateboard") {
			isTriggerSkateboard = true;
		}
	}
			
	private void SkateToTheEnd() {
		isSkating = true;
		mSkateboard.GetComponent<SpriteRenderer>().enabled = false;
		mSkateboardSkating.GetComponent<SpriteRenderer>().enabled = true;
		gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y + 0.5f, gameObject.transform.position.z);

		OGAudioMgr.Instance.PlaySfx(m_skatingSFX);
		LeanTween.moveLocalX (gameObject, mBox.transform.position.x - gameObject.GetComponent<SpriteRenderer> ().bounds.size.x / 3, 5).setOnComplete(onSkatingAnimEnd);
	}

	private void onSkatingAnimEnd() {
		mSkateboardSkating.GetComponent<SpriteRenderer>().enabled = false;
	}

	public void OnTriggerExit2D(Collider2D obj){
		if(obj.tag =="tree"){
			isTriggerTree = false;
		}else if(obj.tag =="ball"){
			isTriggerBall = false;
		}else if(obj.tag =="plank"){
			isTriggerPlank = false;
		}else if(obj.tag =="trashcan"){
			isTriggerTrashcan = false;
		}else if(obj.tag =="bone"){
			isTriggerBone = false;
		}else if(obj.tag =="pole"){
			isTriggerPole = false;
		}else if(obj.tag =="tree2"){
			isTriggerTree2 = false;
		}else if(obj.tag =="tree3"){
			isTriggerTree3 = false;
		}else if (obj.tag == "Skateboard") {
			isTriggerSkateboard = false;
		}
	}

	void OnTouchBox(GameObject box){
		StopWlak ();
		box.GetComponent<Animator> ().SetTrigger ("GoBox");
		LeanTween.alpha (mDarkPanel, 1, 3).setOnComplete(ReloadScene);
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		mCollerOnDog.SetActive (false);
	}
		
	void ReloadScene(){
		SceneManager.LoadScene ("DogdaysNew");
	}

	void EndGame(){ //happy ending
		GetComponent<SpriteRenderer>().enabled = false;
		mCollerOnDog.SetActive (false);
		mGameScene.GetComponent<GameScene> ().PlayFireWork ();
	}

	public void setIsStucking(bool isStucking) {
		mIsStucking = isStucking;
	}
}
